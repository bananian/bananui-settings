#ifndef _SETTINGS_H_
#define _SETTINGS_H_

enum net_usb_mode {
	NET_USB_MODE_DISABLED,
	NET_USB_MODE_TETHER,
	NET_USB_MODE_CLIENT,
};

enum net_state {
	NET_UNAVAILABLE,
	NET_DISCONNECTED,
	NET_CONNECTING,
	NET_AUTHENTICATING,
	NET_IP_CONFIG,
	NET_CONNECTED,
};

#define NET_STATE_TO_STRING(_st) ((_st) == NET_UNAVAILABLE ? "Unavailable" \
		: (_st) == NET_DISCONNECTED ? "Disconnected" \
		: (_st) == NET_CONNECTING ? "Connecting..." \
		: (_st) == NET_AUTHENTICATING ? "Authenticating..." \
		: (_st) == NET_IP_CONFIG ? "Obtaining IP address..." \
		: (_st) == NET_CONNECTED ? "Connected" : "Unknown")

enum net_wlan_sec {
	NET_WLAN_SEC_NONE,
	NET_WLAN_SEC_WEP,
	NET_WLAN_SEC_WPA_PSK,
	NET_WLAN_SEC_WPA_EAP,
};

enum net_type {
	NET_USB,
	NET_WLAN,
	NET_WLAN_SCAN_RESULT,
	NET_WLAN_SCAN_REMOVE_RESULT,
};

struct sbe_net_scan_result {
	const void *private;
	char *ssid, *bssid;
	int active, strength;
};

struct sbe_net {
	void *private, *userdata;
	void (*callback)(void *, enum net_type);
	enum net_usb_mode usb_mode;
	enum net_state usb_state, wlan_state;
	int wlan_enabled;
	struct sbe_net_ops *ops;
};

struct sbe_net_ops {
	int (*setUSBMode)(struct sbe_net *, enum net_usb_mode mode);
	int (*enableWLAN)(struct sbe_net *);
	int (*disableWLAN)(struct sbe_net *);
	int (*scanWLAN)(struct sbe_net *, int report);
	int (*isActiveAP)(struct sbe_net *, struct sbe_net_scan_result *);
	int (*connectWLAN)(struct sbe_net *, struct sbe_net_scan_result *);
	int (*connectHiddenWLAN)(struct sbe_net *, enum net_wlan_sec sec,
			const char *ssid);
	int (*disconnectWLAN)(struct sbe_net *);
	int (*isSameScanResult)(struct sbe_net *, struct sbe_net_scan_result *);
	int (*formatScanResult)(struct sbe_net *, struct sbe_net_scan_result *);
};

#endif
