#ifndef _NETWORKMANAGER_H_
#define _NETWORKMANAGER_H_
#include <bananui/settings.h>
#include "settings.h"

struct sbe_net *createNetworkManagerBackend(bSettings *settings);

#endif
