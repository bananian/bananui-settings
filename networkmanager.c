#include <stdlib.h>
#include <NetworkManager.h>
#include "networkmanager.h"

struct nm_backend {
	NMClient *client;
	NMDevice *usb_device, *wlan_device;
	NMAccessPoint *current_ap;
};

static void set_usb_client_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_client_activate_connection_finish(nm->client, res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully activated USB connection\n");
}

static void set_usb_disconnect_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_device_disconnect_finish(nm->usb_device, res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully disconnected USB\n");
}

static int nmSetUSBMode(struct sbe_net *net, enum net_usb_mode mode)
{
	struct nm_backend *nm = net->private;
	if(!nm->usb_device) return -1;
	if(mode == NET_USB_MODE_DISABLED){
		nm_device_disconnect_async(nm->usb_device,
				NULL,
				set_usb_disconnect_callback,
				net);
	}
	else {
		/* TODO: tethering */
		nm_client_activate_connection_async(nm->client,
				NULL,
				nm->usb_device,
				NULL,
				NULL,
				set_usb_client_callback,
				net);
	}
	return 0;
}

static void set_wlan_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_client_dbus_set_property_finish(nm->client, res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully enabled/disabled WLAN\n");
}

static void wlan_scan_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_device_wifi_request_scan_finish(
			NM_DEVICE_WIFI(nm->wlan_device), res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully triggered WLAN rescan\n");
}

static void wlan_scan_report(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_device_wifi_request_scan_finish(
			NM_DEVICE_WIFI(nm->wlan_device), res, &error);
	if(error) g_warning("%s", error->message);
	else {
		const GPtrArray *aps = nm_device_wifi_get_access_points(
				NM_DEVICE_WIFI(nm->wlan_device));
		int i;
		for(i = 0; i < aps->len; i++){
			nm->current_ap = g_ptr_array_index(aps, i);
			net->callback(net->userdata, NET_WLAN_SCAN_RESULT);
		}
	}
}

static void wlan_connect_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_client_activate_connection_finish(
			nm->client, res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully connected WLAN\n");
}

static void wlan_disconnect_callback(GObject *obj, GAsyncResult *res, gpointer data)
{
	struct sbe_net *net = data;
	struct nm_backend *nm = net->private;
	GError *error = NULL;
	nm_device_disconnect_finish(nm->wlan_device, res, &error);
	if(error) g_warning("%s", error->message);
	else g_print("Successfully disconnected WLAN\n");
}

static int nmSetWLAN(struct sbe_net *net, int state)
{
	struct nm_backend *nm = net->private;
	nm_client_dbus_set_property(nm->client,
			NM_DBUS_PATH,
			NM_DBUS_INTERFACE,
			"WirelessEnabled",
			g_variant_new_boolean(state),
			-1,
			NULL,
			set_wlan_callback,
			net);
	return 0;
}

static int nmEnableWLAN(struct sbe_net *net)
{
	return nmSetWLAN(net, 1);
}

static int nmDisableWLAN(struct sbe_net *net)
{
	return nmSetWLAN(net, 0);
}

static int nmScanWLAN(struct sbe_net *net, int report)
{
	struct nm_backend *nm = net->private;
	nm_device_wifi_request_scan_async(
			NM_DEVICE_WIFI(nm->wlan_device),
			NULL,
			report ? wlan_scan_report : wlan_scan_callback,
			net);
	return 0;
}

static int nmIsActiveAP(struct sbe_net *net, struct sbe_net_scan_result *sr)
{
	struct nm_backend *nm = net->private;
	NMAccessPoint *ap = nm_device_wifi_get_active_access_point(
			NM_DEVICE_WIFI(nm->wlan_device));
	return ap && 0 == strcmp((const char*)sr->private,
			nm_object_get_path(NM_OBJECT(ap)));
}

static int nmConnectWLAN(struct sbe_net *net, struct sbe_net_scan_result *ap)
{
	struct nm_backend *nm = net->private;
	nm_client_activate_connection_async(nm->client,
			NULL,
			nm->wlan_device,
			ap->private,
			NULL,
			wlan_connect_callback,
			net);
	return 0;
}

static int nmIsSameScanResult(struct sbe_net *net, struct sbe_net_scan_result *sr)
{
	struct nm_backend *nm = net->private;
	return 0 == strcmp((const char*)sr->private,
			nm_object_get_path(NM_OBJECT(nm->current_ap)));
}

static int nmFormatScanResult(struct sbe_net *net,
		struct sbe_net_scan_result *sr)
{
	const GBytes *ssid;
	struct nm_backend *nm = net->private;

	sr->private = nm_object_get_path(NM_OBJECT(nm->current_ap));
	sr->bssid = strdup(nm_access_point_get_bssid(nm->current_ap));
	ssid = nm_access_point_get_ssid(nm->current_ap);
	if(ssid){
		const char *data;
		gsize size;
		data = g_bytes_get_data((GBytes*)ssid, &size);
		sr->ssid = strndup(data, size);
	}
	else {
		sr->ssid = strdup("");
	}
	sr->strength = nm_access_point_get_strength(nm->current_ap);
	return 0;
}

static int nmDisconnectWLAN(struct sbe_net *net)
{
	struct nm_backend *nm = net->private;
	nm_device_disconnect_async(nm->wlan_device,
			NULL,
			wlan_disconnect_callback,
			net);
	return 0;
}

static struct sbe_net_ops networkmanager_ops = {
	.setUSBMode = nmSetUSBMode,
	.enableWLAN = nmEnableWLAN,
	.disableWLAN = nmDisableWLAN,
	.scanWLAN = nmScanWLAN,
	.isActiveAP = nmIsActiveAP,
	.connectWLAN = nmConnectWLAN,
	.disconnectWLAN = nmDisconnectWLAN,
	.isSameScanResult = nmIsSameScanResult,
	.formatScanResult = nmFormatScanResult,
};

static enum net_state net_state_from_nm_state(guint state)
{
	switch(state){
		case NM_DEVICE_STATE_PREPARE:
		case NM_DEVICE_STATE_CONFIG:
		case NM_DEVICE_STATE_SECONDARIES:
			return NET_CONNECTING;
		case NM_DEVICE_STATE_NEED_AUTH:
			return NET_AUTHENTICATING;
		case NM_DEVICE_STATE_IP_CONFIG:
		case NM_DEVICE_STATE_IP_CHECK:
			return NET_IP_CONFIG;
		case NM_DEVICE_STATE_DISCONNECTED:
			return NET_DISCONNECTED;
		case NM_DEVICE_STATE_ACTIVATED:
			return NET_CONNECTED;
		default:
			break;
	}
	return NET_UNAVAILABLE;
}

static void usb_state_changed(NMDevice *device, guint new_state,
		guint old_state, guint reason, struct sbe_net *net)
{
	net->usb_state = net_state_from_nm_state(new_state);
	/* TODO: tethering */
	net->usb_mode = net->usb_state == NET_CONNECTED
		? NET_USB_MODE_CLIENT : NET_USB_MODE_DISABLED;
	net->callback(net->userdata, NET_USB);
}

static void wlan_state_changed(NMDevice *device, guint new_state,
		guint old_state, guint reason, struct sbe_net *net)
{
	net->wlan_state = net_state_from_nm_state(new_state);
	net->callback(net->userdata, NET_WLAN);
}

static void wlan_ap_added(NMDeviceWifi *device, NMAccessPoint *ap,
		struct sbe_net *net)
{
	struct nm_backend *nm = net->private;
	nm->current_ap = ap;
	net->callback(net->userdata, NET_WLAN_SCAN_RESULT);
}

static void wlan_ap_removed(NMDeviceWifi *device, NMAccessPoint *ap,
		struct sbe_net *net)
{
	struct nm_backend *nm = net->private;
	nm->current_ap = ap;
	net->callback(net->userdata, NET_WLAN_SCAN_REMOVE_RESULT);
}

static void wlan_enabled_or_disabled(GObject *self, GParamSpec *pspec,
		struct sbe_net *net)
{
	struct nm_backend *nm = net->private;
	net->wlan_enabled = nm_client_wireless_get_enabled(nm->client) ||
		!nm_client_wireless_hardware_get_enabled(nm->client);
	net->callback(net->userdata, NET_WLAN);
}

struct sbe_net *createNetworkManagerBackend(bSettings *settings)
{
	char *if_name;
	struct nm_backend *nm;
	struct sbe_net *net;
	net = calloc(1, sizeof(struct sbe_net));
	if(!net) return NULL;

	nm = calloc(1, sizeof(struct nm_backend));
	if(!nm){
		free(net);
		return NULL;
	}

	nm->client = nm_client_new(NULL, NULL);
	if(!nm_client_get_nm_running(nm->client)){
		g_critical("NetworkManager not running");
		g_object_unref(nm->client);
		free(nm);
		free(net);
		return NULL;
	}

	if_name = bGetSettingString(settings, "net.usb.ifname", "usb0");
	nm->usb_device = nm_client_get_device_by_iface(nm->client, if_name);
	if(!nm->usb_device){
		g_critical("USB device (%s) not found", if_name);
		net->usb_state = NET_UNAVAILABLE;
		net->usb_mode = NET_USB_MODE_DISABLED;
	}
	else {
		net->usb_state = net_state_from_nm_state(
				nm_device_get_state(nm->usb_device));
		/* TODO: tethering */
		net->usb_mode = net->usb_state == NET_CONNECTED
			? NET_USB_MODE_CLIENT : NET_USB_MODE_DISABLED;

		g_signal_connect(nm->usb_device, "state-changed",
				G_CALLBACK(usb_state_changed), net);
	}
	free(if_name);

	if_name = bGetSettingString(settings, "net.wlan.ifname", "wlan0");
	nm->wlan_device = nm_client_get_device_by_iface(nm->client, if_name);
	if(!nm->wlan_device){
		g_critical("WLAN device (%s) not found", if_name);
		net->wlan_state = NET_UNAVAILABLE;
	}
	else if(!NM_IS_DEVICE_WIFI(nm->wlan_device)){
		g_critical("%s is not a WLAN device", if_name);
		net->wlan_state = NET_UNAVAILABLE;
		g_object_unref(nm->wlan_device);
		nm->wlan_device = NULL;
	}
	else {
		net->wlan_state = net_state_from_nm_state(
				nm_device_get_state(nm->wlan_device));
		g_signal_connect(nm->wlan_device, "state-changed",
				G_CALLBACK(wlan_state_changed), net);
		g_signal_connect(nm->wlan_device, "access-point-added",
				G_CALLBACK(wlan_ap_added), net);
		g_signal_connect(nm->wlan_device, "access-point-removed",
				G_CALLBACK(wlan_ap_removed), net);
	}
	free(if_name);

	net->wlan_enabled = nm_client_wireless_get_enabled(nm->client) ||
		!nm_client_wireless_hardware_get_enabled(nm->client);
	g_signal_connect(nm->client, "notify::wireless-enabled",
			G_CALLBACK(wlan_enabled_or_disabled), net);
	g_signal_connect(nm->client, "notify::wireless-hardware-enabled",
			G_CALLBACK(wlan_enabled_or_disabled), net);

	net->private = nm;
	net->ops = &networkmanager_ops;
	return net;
}
