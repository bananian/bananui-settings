#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <gio/gio.h>
#include <bananui/window.h>
#include <bananui/menu.h>
#include "settings.h"
#include "networkmanager.h"

#define MAX_MENU_DEPTH 8

struct wifi_ap_button {
	struct sbe_net_scan_result ap;
	struct application_state *as;
	bButtonWidget *btn;
	struct wifi_ap_button *prev;
};

struct application_state {
	char *section;
	struct sbe_net *net;
	bWindow *wnd;
	bMenu *menu;
	bButtonWidget *net_usb_disable_btn, *net_usb_tether_btn, *net_usb_client_btn,
		      *net_usb_status_btn, *net_wlan_toggle_btn;
	bBoxWidget *scan_text;
	bView *scan_view;
	enum {
		SCAN_NONE,
		SCAN_BT_NEARBY,
		SCAN_BT_PAIRED,
		SCAN_WLAN,
	} scan_type;
	union {
		struct wifi_ap_button *last_wifi_ap;
	};
	bSettings *settings;
	GApplication *gapp;
};

static int usbnet_mode(bMenu *m, void *arg)
{
	enum net_usb_mode mode = (enum net_usb_mode)arg;
	struct application_state *as = m->userdata;
	as->net->ops->setUSBMode(as->net, mode);
	return 1;
}

static void usbnet_mode_update(struct application_state *as)
{
	if(as->net_usb_status_btn){
		bSetTextContent(as->net_usb_status_btn->subtitle,
				NET_STATE_TO_STRING(as->net->usb_state),
				PANGO_WEIGHT_NORMAL, 14);
	}
	if(as->net_usb_disable_btn){
		bSetButtonState(as->net_usb_disable_btn,
				as->net->usb_mode == NET_USB_MODE_DISABLED);
	}
	if(as->net_usb_tether_btn){
		bSetButtonState(as->net_usb_tether_btn,
				as->net->usb_mode == NET_USB_MODE_TETHER);
	}
	if(as->net_usb_client_btn){
		bSetButtonState(as->net_usb_client_btn,
				as->net->usb_mode == NET_USB_MODE_CLIENT);
	}
	bRedrawWindow(as->wnd);
}

static int usbnet_mode_btn(bMenu *m, bButtonWidget *btn, void *arg)
{
	enum net_usb_mode mode = (enum net_usb_mode)arg;
	struct application_state *as = m->userdata;
	switch(mode){
		case NET_USB_MODE_DISABLED:
			as->net_usb_disable_btn = btn;
			break;
		case NET_USB_MODE_TETHER:
			as->net_usb_tether_btn = btn;
			break;
		case NET_USB_MODE_CLIENT:
			as->net_usb_client_btn = btn;
			break;
		default:
			as->net_usb_status_btn = btn;
			if(btn) bSetTextContent(btn->subtitle,
					NET_STATE_TO_STRING(as->net->usb_state),
					PANGO_WEIGHT_NORMAL, 14);
			return 1; /* don't set button state */
	}
	if(btn) bSetButtonState(btn, mode == as->net->usb_mode);
	return 1;
}

static int wlan_toggle(bMenu *m, void *arg)
{
	struct application_state *as = m->userdata;
	if(as->net->wlan_enabled){
		as->net->ops->disableWLAN(as->net);
	}
	else {
		as->net->ops->enableWLAN(as->net);
	}
	return 1;
}

static int wlan_toggle_btn(bMenu *m, bButtonWidget *btn, void *arg)
{
	struct application_state *as = m->userdata;
	as->net_wlan_toggle_btn = btn;
	if(btn) bSetTextContent(btn->subtitle,
			as->net->wlan_enabled
			? NET_STATE_TO_STRING(as->net->wlan_state)
			: "Disabled",
			PANGO_WEIGHT_NORMAL, 14);
	if(btn) bSetButtonState(btn, as->net->wlan_enabled);
	return 1;
}

static void wlan_state_update(struct application_state *as)
{
	if(as->net_wlan_toggle_btn){
		bSetTextContent(as->net_wlan_toggle_btn->subtitle,
				as->net->wlan_enabled
				? NET_STATE_TO_STRING(as->net->wlan_state)
				: "Disabled",
				PANGO_WEIGHT_NORMAL, 14);
		bSetButtonState(as->net_wlan_toggle_btn,
				as->net->wlan_enabled);
		bRedrawWindow(as->wnd);
	}
}

static int handle_wifi_ap_click(void *param, void *data)
{
	struct wifi_ap_button *click = data;
	if(click->as->net->ops->isActiveAP(click->as->net, &click->ap)){
		click->as->net->ops->disconnectWLAN(click->as->net);
		bSetSoftkeyText(click->as->menu->current->view->sk,
				"", "CONNECT", "");
		bRedrawWindow(click->as->wnd);
		return 1;
	}
	else {
		click->as->net->ops->connectWLAN(click->as->net,
				&click->ap);
		/* exit back to Wi-Fi menu */
		bPopMenu(click->as->menu);
		/* bPopMenu manipulates event data, stop event processing */
		return 0;
	}
}

static int handle_wifi_ap_focusin(void *param, void *data)
{
	struct wifi_ap_button *click = data;
	if(click->as->net->ops->isActiveAP(click->as->net, &click->ap)){
		bSetSoftkeyText(click->as->menu->current->view->sk,
				"", "DISCONNECT", "Rescan");
	}
	else {
		bSetSoftkeyText(click->as->menu->current->view->sk,
				"", "CONNECT", "Rescan");
	}
	/* button focus handler will redraw the window */
	return 1;
}

static int handle_wifi_ap_destroy(void *param, void *data)
{
	struct wifi_ap_button *click = data;
	free(click->ap.bssid);
	free(click->ap.ssid);
	free(click);
	return 1;
}

static void wlan_scan_remove_result(struct application_state *as)
{
	struct wifi_ap_button **ptr, *click;
	for(ptr = &as->last_wifi_ap; *ptr; ptr = &click->prev){
		click = *ptr;
		if(as->net->ops->isSameScanResult(as->net, &click->ap)){
			*ptr = click->prev;
			if(as->scan_view->selected &&
				as->scan_view->selected->box ==
				click->btn->box && click->prev)
			{
				bSetFocus(as->scan_view,
					click->prev->btn->box, 0);
			}
			bDestroyBoxRecursive(click->btn->box);
			bRedrawWindow(as->wnd);
			return;
		}
	}
}

static void wlan_scan_show_result(struct application_state *as)
{
	struct wifi_ap_button *click, *next;
	bButtonWidget *btn;

	if(as->scan_text){
		bDestroyBoxRecursive(as->scan_text);
		as->scan_text = NULL;
	}

	click = malloc(sizeof(struct wifi_ap_button));
	if(!click) return;

	as->net->ops->formatScanResult(as->net, &click->ap);
	click->as = as;

	btn = bCreateButtonWidget(
			click->ap.ssid ? click->ap.ssid : "",
			B_BUTTON_STYLE_ICON,
			B_ALIGN_START,
			-1, 60);
	if(!btn){
		free(click->ap.bssid);
		free(click->ap.ssid);
		free(click);
		return;
	}

	click->btn = btn;

	bSetTextContent(btn->subtitle, click->ap.bssid,
			PANGO_WEIGHT_NORMAL, 14);
	bLoadImageFromIcon(btn->icon, click->ap.strength <= 20
		? "network-wireless-signal-none-symbolic"
		: click->ap.strength <= 40
		? "network-wireless-signal-weak-symbolic"
		: click->ap.strength <= 60
		? "network-wireless-signal-ok-symbolic"
		: click->ap.strength <= 80
		? "network-wireless-signal-good-symbolic"
		: "network-wireless-signal-excellent-symbolic",
		24);
	bRegisterEventHandler(&btn->box->click,
			handle_wifi_ap_click, click);
	bRegisterEventHandler(&btn->box->focusin,
			handle_wifi_ap_focusin, click);
	bRegisterEventHandler(&btn->box->destroy,
			handle_wifi_ap_destroy, click);

	if(!as->last_wifi_ap ||
		click->ap.strength <= as->last_wifi_ap->ap.strength)
	{
		/* append at the end */
		click->prev = as->last_wifi_ap;
		as->last_wifi_ap = click;
		bAddBoxToBox(as->scan_view->body, btn->box);
	}
	else {
		next = as->last_wifi_ap;
		while(next->prev && click->ap.strength > next->prev->ap.strength)
			next = next->prev;
		click->prev = next->prev;
		next->prev = click;
		bInsertBox(btn->box, next->btn->box);
		bRedrawWindow(as->wnd);
		if(as->scan_view->selected &&
			as->scan_view->selected->box ==
			next->btn->box)
		{
			bSetFocus(as->scan_view,
				click->btn->box, 0);
		}
	}

	bRedrawWindow(as->wnd);
}

static int wlan_scan_keydown(void *param, void *data)
{
	xkb_keysym_t *sym = param;
	struct application_state *as = data;
	if(*sym == BANANUI_KEY_SoftRight)
		as->net->ops->scanWLAN(as->net, 0);
	return 1;
}

static int unbind_scan_results(void *param, void *data)
{
	struct application_state *as = data;
	as->scan_type = SCAN_NONE;
	as->scan_view = NULL;
	return 1;
}

/*
B_MENU_ITEMS(bt_name) = {};
B_DEFINE_MENU(bt_name) {
	bSetMenuTitle(m, "Change device name");
}
B_MENU_ITEMS(bt_scan) = {};
B_DEFINE_MENU(bt_scan) {
	struct application_state *as = m->userdata;
	bSetMenuTitle(m, "Nearby devices");
	as->scan_type = SCAN_BT_NEARBY;
	as->scan_view = m->current->view;
	bRegisterEventHandler(&m->current->view->body->destroy,
			unbind_scan_results, as);
}
B_MENU_ITEMS(bt_paired) = {};
B_DEFINE_MENU(bt_paired) {
	struct application_state *as = m->userdata;
	bSetMenuTitle(m, "Paired devices");
	as->scan_type = SCAN_BT_PAIRED;
	as->scan_view = m->current->view;
	bRegisterEventHandler(&m->current->view->body->destroy,
			unbind_scan_results, as);
}
B_MENU_ITEMS(bluetooth) = {
	{
		.name = "Bluetooth",
		.style = B_BUTTON_STYLE_CHECKBOX,
		//.click_handler = bt_toggle,
		//.button_handler = bt_toggle_btn,
	},
	B_SUBMENU(bt_scan, "Nearby devices"),
	B_SUBMENU(bt_paired, "Paired devices"),
	{
		.name = "Visible to others",
		.style = B_BUTTON_STYLE_CHECKBOX,
		//.click_handler = bt_disc_toggle,
		//.button_handler = bt_disc_toggle_btn,
	},
	B_SUBMENU(bt_name, "Change device name"),
};
B_DEFINE_MENU(bluetooth) {
	bSetMenuTitle(m, "Bluetooth");
}*/
B_MENU_ITEMS(wlan_scan) = {};
B_DEFINE_MENU(wlan_scan) {
	bContentWidget *scan_text_content;
	struct application_state *as = m->userdata;
	bSetMenuTitle(m, "Available networks");
	as->last_wifi_ap = NULL;
	as->scan_type = SCAN_WLAN;
	as->scan_view = m->current->view;
	bRegisterEventHandler(&m->current->view->body->destroy,
			unbind_scan_results, as);
	bRegisterEventHandler(&m->current->view->keydown,
			wlan_scan_keydown, as);
	as->scan_text = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 0);
	if(as->scan_text){
		scan_text_content = bCreateContentWidget(B_ALIGN_CENTER, 0);
		if(scan_text_content){
			scan_text_content->color = bColorFromTheme("text");
			bSetTextContent(scan_text_content,
				as->net->wlan_enabled ?
				(as->net->wlan_state == NET_UNAVAILABLE
				 ? "Wi-Fi is unavailable" : "Scanning...")
				: "Wi-Fi is disabled",
				PANGO_WEIGHT_NORMAL, 14);
			bAddContentToBox(as->scan_text, scan_text_content);
		}
		bAddBoxToBox(as->scan_view->body, as->scan_text);
	}
	if(as->net->wlan_state != NET_UNAVAILABLE){
		as->net->ops->scanWLAN(as->net, 1);
	}
}
/*B_MENU_ITEMS(wlan_hidden) = {};
B_DEFINE_MENU(wlan_hidden) {
	bContentWidget *prompt;
	bInputWidget *inp;
	bSetSoftkeyText(m->current->view->sk, "", "SELECT", "Connect");
	bSetMenuTitle(m, "Connect to hidden Wi-Fi");
	prompt = bCreateContentWidget(B_ALIGN_START, 0);
	if(prompt){
		bSetTextContent(prompt, " SSID:",
				PANGO_WEIGHT_NORMAL, 14);
		bAddContentToBox(m->current->view->body, prompt);
		prompt->color = bColorFromTheme("text");
	}

	inp = bCreateInputWidget(m->wnd, B_INPUT_STYLE_SINGLELINE,
			B_INPUT_MODE_ALPHA);
	if(inp){
		bAddBoxToBox(m->current->view->body, inp->box);
	}
};*/
B_MENU_ITEMS(wlan) = {
	{
		.name = "Wi-Fi",
		.style = B_BUTTON_STYLE_CHECKBOX,
		.click_handler = wlan_toggle,
		.button_handler = wlan_toggle_btn,
	},
	B_SUBMENU(wlan_scan, "Available networks"),
	/*B_SUBMENU(wlan_hidden, "Connect to hidden Wi-Fi"),*/
};
B_DEFINE_MENU(wlan) {
	bSetMenuTitle(m, "Wi-Fi");
}
/*B_MENU_ITEMS(mobile) = {};
B_DEFINE_MENU(mobile) {
	bSetMenuTitle(m, "Mobile network");
}*/
B_MENU_ITEMS(usbnet) = {
	{
		.name = "Disabled",
		.style = B_BUTTON_STYLE_RADIO,
		.click_handler = usbnet_mode,
		.button_handler = usbnet_mode_btn,
		.arg = (void*)NET_USB_MODE_DISABLED,
	},
	/*{
		.name = "Tethering mode (TODO)",
		.style = B_BUTTON_STYLE_RADIO,
		.click_handler = usbnet_mode,
		.button_handler = usbnet_mode_btn,
		.arg = (void*)NET_USB_MODE_TETHER,
	},*/
	{
		.name = "Client mode",
		.style = B_BUTTON_STYLE_RADIO,
		.click_handler = usbnet_mode,
		.button_handler = usbnet_mode_btn,
		.arg = (void*)NET_USB_MODE_CLIENT,
	},
	{
		.name = "USB state",
		.style = B_BUTTON_STYLE_MENU,
		.button_handler = usbnet_mode_btn,
		.arg = (void*)-1,
	},
};
B_DEFINE_MENU(usbnet) {
	bSetMenuTitle(m, "USB network");
}
B_MENU_ITEMS(network) = {
	B_SUBMENU(wlan, "Wi-Fi"),
	/*B_SUBMENU(bluetooth, "Bluetooth (TODO)"),
	B_SUBMENU(mobile, "Mobile network (TODO)"),*/
	B_SUBMENU(usbnet, "USB network"),
};
B_DEFINE_MENU(network) {
	bSetMenuTitle(m, "Network options");
}
/*B_MENU_ITEMS(themes) = {};
B_DEFINE_MENU(themes) {
	bSetMenuTitle(m, "Themes");
}
B_MENU_ITEMS(personal) = {
	B_SUBMENU(themes, "Themes"),
};
B_DEFINE_MENU(personal) {
	bSetMenuTitle(m, "Personalization");
}*/
B_MENU_ITEMS(mm) = {
	B_SUBMENU(network, "Network"),
	/*B_SUBMENU(personal, "Personalization (TODO)"),*/
};
B_DEFINE_MENU(mm) {
	bSetMenuTitle(m, "Settings");
}

static void net_callback(void *data, enum net_type net)
{
	struct application_state *as = data;
	switch(net){
		case NET_USB:
			usbnet_mode_update(as); break;
		case NET_WLAN:
			wlan_state_update(as); break;
		case NET_WLAN_SCAN_REMOVE_RESULT:
			if(as->scan_type != SCAN_WLAN) return;
			wlan_scan_remove_result(as);
			break;
		case NET_WLAN_SCAN_RESULT:
			if(as->scan_type != SCAN_WLAN) return;
			wlan_scan_show_result(as);
			break;
	}
}

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	xkb_keysym_t *sym = param;

	if(*sym == BANANUI_KEY_EndCall){
		g_application_quit(as->gapp);
		return 1;
	}

	if(*sym == BANANUI_KEY_Back){
		if(bPopMenu(as->menu) == 0){
			g_application_quit(as->gapp);
		}
		/*
		 * bPopMenu manipulates events, so event processing
		 * has to be stopped here.
		 */
		return 0;
	}

	return 1;
}

static gboolean window_io(GIOChannel *source, GIOCondition condition,
		gpointer wnd)
{
	bHandleWindowEvent(wnd);
	return TRUE;
}

static gint app_command_line(GApplication *self, GVariantDict *options,
		struct application_state *as)
{
	char *sect;
	if(g_variant_dict_lookup(options, "section", "s", &sect)){
		as->section = strdup(sect);
	}
	return -1;
}

static void app_activate(GApplication *self, struct application_state *as)
{
	GIOChannel *iochannel;
	if(as->wnd){
		/* Bring to foreground */
		bWindow *tmp = bCreateWindow("Settings popup");
		bRedrawWindow(tmp);
		bDestroyWindow(tmp);
		return;
	}
	as->wnd = bCreateWindow("Settings");
	if(!as->wnd){
		g_error("Failed to create window");
		g_application_quit(self);
	}

	iochannel = g_io_channel_unix_new(bGetWindowFd(as->wnd));
	g_io_add_watch(iochannel, G_IO_IN | G_IO_HUP | G_IO_ERR, window_io, as->wnd);
	g_application_hold(as->gapp);

	bRegisterEventHandler(&as->wnd->keydown, handle_keydown, as);

	as->menu = bCreateMenuView(as->wnd);
	as->menu->userdata = as;
	if(as->section){
		if(0 == strcmp(as->section, "network"))
			B_OPEN_MENU(as->menu, network);
		else if(0 == strcmp(as->section, "wifi"))
			B_OPEN_MENU(as->menu, wlan);
		/*else if(0 == strcmp(as->section, "bluetooth"))
			B_OPEN_MENU(as->menu, bluetooth);
		else if(0 == strcmp(as->section, "mobile"))
			B_OPEN_MENU(as->menu, mobile);*/
		else if(0 == strcmp(as->section, "usbnet"))
			B_OPEN_MENU(as->menu, usbnet);
		/*else if(0 == strcmp(as->section, "personalization"))
			B_OPEN_MENU(as->menu, personal);
		else if(0 == strcmp(as->section, "themes"))
			B_OPEN_MENU(as->menu, themes);*/
		else
			B_OPEN_MENU(as->menu, mm);
	}
	else {
		B_OPEN_MENU(as->menu, mm);
	}

}

int main(int argc, char **argv)
{
	struct application_state as = {};

	bGlobalAppClass = "system";

	as.scan_type = SCAN_NONE;
	as.settings = bCreateSettingsManager();

	as.gapp = g_application_new("bananian.bananui.Settings",
			G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect(as.gapp, "activate",
			G_CALLBACK(app_activate), &as);
	g_signal_connect(as.gapp, "handle-local-options",
			G_CALLBACK(app_command_line), &as);

	g_application_add_main_option(as.gapp,
			"section",
			's',
			G_OPTION_FLAG_NONE,
			G_OPTION_ARG_STRING,
			"The section to be opened on startup",
			"SECTION");

	as.net = createNetworkManagerBackend(as.settings);
	as.net->callback = net_callback;
	as.net->userdata = &as;

	return g_application_run(as.gapp, argc, argv);
}
