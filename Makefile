PKG_CONFIG = pkg-config
CFLAGS += -Wall -g $(shell $(PKG_CONFIG) --cflags libwbananui1 gio-2.0 libnm)
LDFLAGS += $(shell $(PKG_CONFIG) --libs libwbananui1 gio-2.0 libnm)
OBJECTS = settings.o networkmanager.o
OUTPUTS = bananui-settings

all: $(OUTPUTS)

bananui-settings: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS) $(CFLAGS)

settings.o: networkmanager.h settings.h
networkmanager.o: networkmanager.h settings.h

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) $(OUTPUTS)

install:
	install -Dm755 $(OUTPUTS) -t $(DESTDIR)/usr/bin
	install -Dm644 settings.desktop $(DESTDIR)/usr/share/applications/bananui-settings.desktop
